# Final Project Laravel - SanberCode

## Kelompok 1

## Anggota Kelompok

-   Wahyu Santoso
-   Muhammad Ihsan

## Tema Project

Web forum diskusi

## ERD

![ERD_Final_Project_Fix.png](ERD_Final_Project_Fix.png?raw=true)

## Link Video

Link Deploy :http://sanyu.sanbercodeapp.com/

Link Demo Aplikasi :https://youtu.be/GNK38omu_hM

Link Template :https://themewagon.com/themes/free-responsive-bootstrap-5-html5-admin-template-sneat/
